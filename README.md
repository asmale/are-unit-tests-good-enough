## Structure of the Repository

The master branch contains the original version of the subject of study. Each
branch in this repository is the subject project with an incorrect design
pattern variant. Each branch contains the original test set. It can be shown
by running `gradle test` that the variants can pass the tests. This addresses
our concern and is the motivation of our project.

The naming convention for branches is `<pattern>-<variant_type>`. The source
folder structure follows the convention of Gradle's Java plugin that the
application code is in `src/main/java` and the test code is in
`src/test/java`. In addition, all our `Mockito` tests are located in
`src/mockitoTest/java`.

## Execution

To access the variants, please checkout the corresponding branch using `git
checkout <name-of-the-branch>`.

To display the difference between the variant and original application: `git
diff master <name-of-the-branch>`.

To run the original tests: `gradle test`

To run the Mockito tests: `gradle mockitoTest`

## Authors

* Austin Smale
* Chen Huo
* Courtney Rush
* Drake Conaway
