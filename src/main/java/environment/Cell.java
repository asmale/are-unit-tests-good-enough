package environment;
import lifeform.LifeForm;
/**
 * A Cell that can hold a LifeForm.
 * @author Dr. Dudley Girard -- first author
 * @author Dr. Alice Armstrong -- revisions
 */
public class Cell
{
  private LifeForm cellLife;  //the LifeForm that exists in thsi cell

 /**
  * @return the LifeForm in this Cell.
  */
    public LifeForm getLifeForm()
    {
        return cellLife;
    }

/**
 * Tries to add the LifeForm to the Cell.  Will not add if a
 * LifeForm is already present.
 * @return true if the LifeForm was added the Cell, false otherwise.
 */
    public boolean addLifeForm(LifeForm entity)
    {
    	//if there is not LifeForm in this cell, put entity here
    	if (cellLife == null)
    	{
    		cellLife = entity;
    		return true;
    	}

    	//otherwise, the cell is already full. Do not place entity here
        return false;
    }

    /**
     * Removes any LifeForm from this Cell.
     * @author Dr. Alice Armstrong
     */
    public void removeLifeForm()
    {
    	cellLife = null;
    }
}

