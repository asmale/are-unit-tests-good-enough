package environment;
import lifeform.LifeForm;

/**
 * @author Dr. Alice Armstrong
 *
 */
public class Environment {
	
	private Cell[][] cells; //the 2D array of cells in the environment
	
	/**
	 * creates a new Environment
	 * @param rows the number of rows in the Environment
	 * @param cols the number of columns in the Environment
	 */
	public Environment(int rows, int cols)
	{
		cells = new Cell[rows][cols]; 
		
		for(int i = 0; i < rows; i++)
		{
			for(int j = 0; j <cols; j++)
			{
				cells[i][j] = new Cell(); 
			}
		}
	}
	
	/**
	 * returns a specified cell in the Environment
	 * @param row
	 * @param col
	 */
	public LifeForm getLifeForm(int row, int col)
	{
		return cells[row][col].getLifeForm(); 
	}

	/**
	 * adds a LifeForm to a Cell in the Environment
	 * @param entity
	 * @param row
	 * @param col
	 * @return true is LifeForm was added
	 */
	public boolean addLifeForm(LifeForm entity, int row, int col)
	{
		return cells[row][col].addLifeForm(entity); 
	}
	
	public void removeLifeForm(int row, int col)
	{
		cells[row][col].removeLifeForm(); 
	}
}
