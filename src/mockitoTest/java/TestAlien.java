import static org.junit.Assert.*;

import java.lang.reflect.Field;

import environment.Range;
import exceptions.RecoveryRateException;
import gameplay.SimpleTimer;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import recovery.RecoveryBehavior;
import recovery.RecoveryFractional;
import recovery.RecoveryLinear;
import recovery.RecoveryNone;

/**
 * @author Dr. Alice Armstrong
 *
 * @author Austin Smale -- revisions
 * @author Drake Conaway -- revisions
 */
public class TestAlien
{

	/**
	 * Check to make sure that the RecoveryBehavior field is of type
	 * RecoveryBehavior, not anything else
	 * 
	 * This test will not pass because recovery in Alien is of type
	 * ArrayList<RecoveryBehavior>, not RecoveryBehavior
	 * @throws Exception
	 *
	 */
	@Test
	public void testToMakeSureAlienUsesCorrectFieldType() throws Exception
	{
		// fields
		RecoveryBehavior recovery = Mockito.spy(RecoveryNone.class);

		// Set the private field to our mocked recovery behavior
		Alien a = new Alien("Tester", 100);
		Field f = Alien.class.getDeclaredField("recovery");
		f.setAccessible(true);
		f.set(a, recovery);

		// should not reach this point of test
		SimpleTimer timer = new SimpleTimer();
		// Alien should implement TimerObserver
		timer.addTimeObserver(a);

		// set to round 1
		timer.timeChanged();

		// AlienBob to take some damage (more than the recovery amount)
		a.takeHit(6);
		Mockito.verify(recovery).calculateRecovery(Mockito.anyInt(), Mockito.anyInt());
	}

	private Range r = new Range();

	/**
	 * ********** LAB 3 (Observer) TESTS START HERE (refactored in Lab 4 to
	 * handle distance
	 *
	 * @throws RecoveryRateException *****************
	 */
	@Test
	/** This tests the creation of an Alien without a recover behavior */
	public void testInitialization() throws RecoveryRateException
	{
		Alien a = new Alien("AlienBob", 15);
		assertEquals(15, a.getMaxLifePoints());
		assertEquals(10, a.getAttackStrength());
	}

	/**
	 * makes sure we can * create a LifeForm with an attack strength * check the
	 * attack strength * attack another LifeForm * see the other LifeForm take
	 * the damage
	 *
	 * @throws RecoveryRateException
	 */
	@Test
	public void testAttack() throws RecoveryRateException
	{
		MockLifeForm bob;
		Alien sheryl;
		bob = new MockLifeForm("Bob", 40, 5);
		sheryl = new Alien("Sheryl", 50);

		// set distance to something in range
		r.distance = 3;

		// check that we can get the attack strength
		assertEquals(5, bob.getAttackStrength());

		// if Bob attacks Sheryl, Sheryl should take 5 points of damage
		bob.attack(sheryl, r.distance);
		assertEquals(45, sheryl.getCurrentLifePoints());

		// if Sheryl attacks Bob, Bob should take 10 points of damage
		sheryl.attack(bob, r.distance);
		assertEquals(30, bob.getCurrentLifePoints());
	}

}
