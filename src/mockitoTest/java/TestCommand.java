import java.awt.event.ActionEvent;

import org.junit.Test;
import org.mockito.Mockito;

import command.AttackCmd;
import command.Command;
import command.Invoker;

/**
 * @author Austin Smale
 * @author Drake Conaway
 */
public class TestCommand
{
	/**
	 * Test to see that the invoker stores the commands and invokes the commands
	 * that it stores
	 */
	@Test
	public void testInvokerStoreCommands()
	{
		Invoker iMocked = Mockito.mock(MockInvoker.class);
		Command cmdMocked = Mockito.mock(AttackCmd.class);

		// simulate a attack command pressed
		ActionEvent e = new ActionEvent(new Object(), 0, "attack");
		iMocked.setAttack(cmdMocked);
		iMocked.actionPerformed(e);

		// verify attack command was executed from the stored invoker command
		Mockito.verify(iMocked, Mockito.times(1)).setAttack(Mockito.any(AttackCmd.class));
		Mockito.verify(cmdMocked, Mockito.times(1)).execute(Mockito.anyInt(), Mockito.anyInt());

	}
	
	class MockInvoker extends Invoker {
		@Override
		public void setAttack(Command cmd)
		{
			super.setAttack(cmd);
		}
	}
}
