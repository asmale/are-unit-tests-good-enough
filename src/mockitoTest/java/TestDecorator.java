import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.lang.reflect.Field;

import org.junit.Test;

import exceptions.AttachmentException;
import weapon.Attachment;
import weapon.Pistol;
import weapon.PowerBooster;
import weapon.Weapon;

public class TestDecorator
{
	/**
	 * Check if the field name is base
	 * 
	 * Should fail
	 * @throws AttachmentException
	 */
	@Test
	public void checkName() throws AttachmentException
	{
		Field[] fields = Attachment.class.getDeclaredFields();
		assertEquals(fields[0].getName(), "base");
	}

	/**
	 * Check to make sure that the PowerBooster class has correct hierarchy
	 * @throws AttachmentException
	 */
	@Test
	public void checkHierarchy() throws AttachmentException
	{
		PowerBooster powerPistol = new PowerBooster(new Pistol());
		Class<?> superclass = powerPistol.getClass().getSuperclass();
		// classes extend Object by default, so checking if a class extends
		// Object means that it does not extend Attachment
		assertNotEquals(Object.class, superclass);
	}
}
