import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.junit.Test;
import org.mockito.Mockito;

import environment.Environment;
import lifeform.Human;
import lifeform.LifeForm;

public class TestEnvironment
{
	/**
	 * Should fail since the constructor of Environment is public and not
	 * private
	 * 
	 * Found online at
	 * stackoverflow.com/questions/4520216/how-to-add-test-coverage-to-a-private-constructor
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	@Test
	public void testPrivateConstructor() throws NoSuchMethodException, SecurityException
	{
		Constructor<Environment> c = Environment.class.getDeclaredConstructor();
		assertTrue(Modifier.isPrivate(c.getModifiers()));
	}

	@Test
	public void testOnlyOneInstanceCreated()
	{
		Environment e = Mockito.spy(Environment.getEnvironment(10, 10));
		LifeForm l = new Human("Test", 10, 10);
		e.addLifeForm(l, 1, 1);
		// verify that lifeform l was added
		Mockito.verify(e, Mockito.times(1)).addLifeForm(Mockito.any(LifeForm.class), Mockito.anyInt(),
				Mockito.anyInt());
		e = Mockito.spy(Environment.getEnvironment(10, 10));

		// check to see if lifeform is there
		assertEquals(l, e.getLifeForm(1, 1));
	}
}
