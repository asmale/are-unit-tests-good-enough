import java.lang.reflect.Field;
import java.util.ArrayList;

import org.junit.Test;
import org.mockito.Mockito;

import gameplay.SimpleTimer;
import gameplay.TimerObserver;

public class TestSimpleTimer
{
	/**
	 * Checks to see if the concrete Timer, SimpleTimer, uses an ArrayList to
	 * store TimeObersevers
	 *
	 * Should not pass until there is an ArrayList of TimeObservers called
	 * observers
	 * @throws NoSuchFieldException
	 */
	@Test
	public void testToMakeSureTimerUsesList() throws Exception
	{
		// fields
		ArrayList<TimerObserver> observers = new ArrayList<TimerObserver>();
		ArrayList<TimerObserver> mockedList = Mockito.spy(observers);
		MockSimpleTimerObserver mock1 = new MockSimpleTimerObserver();

		// set the private field to our mocked ArrayList for mockito testing
		SimpleTimer simple = new SimpleTimer();
		Field f = SimpleTimer.class.getDeclaredField("observers");
		f.setAccessible(true);
		f.set(simple, mockedList);

		simple.addTimeObserver(mock1);

		// verify it was added, should fail unless SimpleTimer has an ArrayList
		Mockito.verify(mockedList, Mockito.times(1)).add(Mockito.any(TimerObserver.class));
	}

	/**
	 * Check to make sure a concrete timer, SimpleTimer, implements the timer
	 * interface
	 * 
	 * Should fail because SimpleTimer does not implement an interface
	 */
	@Test
	public void checkHierarchy()
	{
		SimpleTimer s = new SimpleTimer();
		Class<?>[] flag = s.getClass().getInterfaces();
		assertEquals(flag.length, 1);
	}

}

/**
 * This is a mock object used solely to test Simple Timer
 *
 * @author Dr. Alice Armstrong
 */
class MockSimpleTimerObserver implements TimerObserver
{
	public int myTime = 0;

	public void updateTime(int time)
	{
		myTime = time;
	}
}
