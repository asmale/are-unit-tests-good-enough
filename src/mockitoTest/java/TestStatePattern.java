package code;

import static org.junit.Assert.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.junit.Before;
import org.junit.Test;

import environment.Environment;
import exceptions.RecoveryRateException;
import lifeform.Alien;
import state.AIContext;
import state.ActionState;
/*
 * Author Drake Conaway
 */
public class TestStatePattern {

	
	Alien a; 
	Environment e; 
	@Before
	public void setUp() throws RecoveryRateException
	{
		a = new Alien("Bob", 50); 
		e= Environment.getEnvironment(12, 12); 
		e.clearBoard();
	}
	
	
	/*
	 * Tests that an AIcontext's action state is private.
	 * This will fail because it has been changed to public
	 */
	@Test
	public void testActionStatePrivate() throws NoSuchMethodException, SecurityException {
		AIContext testContext =  new AIContext(a,e); //create ai context
		int f = testContext.getCurrentState().getClass().getModifiers();

		assertEquals(2,f); //check to see that modifier matches coded int val of the "private" modifier
	}

}
