import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import recovery.TestRecovery;
import environment.TestEnvironment;
import environment.TestCell;
import lifeform.TestAlien;
import lifeform.TestLifeForm;
import lifeform.TestHuman;

/**
 * Runs all of the tests in this project
 * @author Dr. Dudley Girard -- first author
 * @author Dr. Alice Armstrong -- revisions
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(
{
 TestLifeForm.class,
 TestCell.class,
 TestEnvironment.class,
 TestHuman.class,
 TestAlien.class,
 TestRecovery.class
})

public class AllGameTests
{
}

