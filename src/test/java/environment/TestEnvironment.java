package environment;
import static org.junit.Assert.*;
import lifeform.MockLifeForm;

import org.junit.Test;

/**
 * @author Dr. Alice Armstrong
 *
 */
public class TestEnvironment 
{
	
	/************ LAB 2 (Strategy) TESTS BELOW THIS POINT ******************/

	/**
	 * checks that we can create a 1x1 Environment without any LifeForms
	 */
	@Test
	public void testInitialization() {
		Environment e = new Environment(1, 1); 
		assertNull(e.getLifeForm(0, 0)); 
	}

	/**
	 * checks that we can add a LifeForm to a mulitdimensional Environment
	 */
	@Test
	public void testAddLifeForm()
	{
		
		Environment e = new Environment(2, 3); 
		MockLifeForm entity; 
        entity = new MockLifeForm("Bob", 40); 
        
        //border case 1,2
        assertTrue(e.addLifeForm(entity, 1, 2));
        MockLifeForm checkLF = (MockLifeForm) e.getLifeForm(1, 2); 
        assertEquals("Bob", checkLF.getName()); 
        assertEquals(40, checkLF.getCurrentLifePoints()); 
        
        //border case 0,2
        assertTrue(e.addLifeForm(entity, 0, 2));
        checkLF = null; 
        checkLF = (MockLifeForm) e.getLifeForm(0, 2); 
        assertEquals("Bob", checkLF.getName()); 
        assertEquals(40, checkLF.getCurrentLifePoints());
        
        //border case 0,0
        assertTrue(e.addLifeForm(entity, 0, 0));
        checkLF = null; 
        checkLF = (MockLifeForm) e.getLifeForm(0, 0); 
        assertEquals("Bob", checkLF.getName()); 
        assertEquals(40, checkLF.getCurrentLifePoints());
        
        //border case 1, 0
        assertTrue(e.addLifeForm(entity, 1, 0));
        checkLF = null; 
        checkLF = (MockLifeForm) e.getLifeForm(1, 0); 
        assertEquals("Bob", checkLF.getName()); 
        assertEquals(40, checkLF.getCurrentLifePoints());
	}
	
	/**
	 * adds a LifeFormto the Environment, then removes it. 
	 * checks that removing from an empty cell is OK
	 */
	@Test
	public void testRemoveLifeForm()
	{
		Environment e = new Environment(2, 3); 
		MockLifeForm entity; 
        entity = new MockLifeForm("Bob", 40); 
        
        //add a LF to 1,2
        assertTrue(e.addLifeForm(entity, 1, 2));
        MockLifeForm checkLF = (MockLifeForm) e.getLifeForm(1, 2); 
        assertEquals("Bob", checkLF.getName()); 
        assertEquals(40, checkLF.getCurrentLifePoints()); 
        
        //remove the LF
        e.removeLifeForm(1,2); 
        assertNull(e.getLifeForm(1, 2)); 
        
        //try removing from the same cell again
        e.removeLifeForm(1,2); 
        assertNull(e.getLifeForm(1, 2));
        
        //try removing from a cell that never had a LF in it
        assertNull(e.getLifeForm(0, 0)); 
        e.removeLifeForm(0, 0); 
        assertNull(e.getLifeForm(0, 0)); 
       
		
	}
}
