package lifeform;
import static org.junit.Assert.*; 

import org.junit.Test; 

import environment.Range;
import gameplay.SimpleTimer;
import weapon.MockWeapon;
import weapon.Pistol;
import weapon.PlasmaCannon;
import weapon.Weapon;
 
/** 
 * Tests the functionality provided by the Book class 
 * 
 */ 
public class TestLifeForm 
{ 

	/************ LAB 4 (Decorator) TESTS START HERE ******************/
	private Range r = new Range(); 
	
	/**
	 * make sure that a LifeForm with no weapon cannot attack
	 * an opponent that is more that 5 feet away
	 */
	@Test
	public void testHandToHandOutOfRange()
	{
		MockLifeForm bob;
		MockLifeForm sheryl; 
		bob = new MockLifeForm("Bob", 40, 5); 
		sheryl = new MockLifeForm("Sheryl", 50, 7); 
		
		//set the distance to an acceptable attack range
		r.distance = 6; 
		
		//if Bob attacks Sheryl, Sheryl should take 0 points of damage
		//because they are too far apart
        bob.attack(sheryl, r.distance); 
        assertEquals(50, sheryl.getCurrentLifePoints()); 
	}
	
	@Test
	public void testAttackWithWeapon()
	{
		MockLifeForm bob;
		MockLifeForm sheryl; 
		bob = new MockLifeForm("Bob", 100, 1); 
		sheryl = new MockLifeForm("Sheryl", 50, 2); 
		
		Pistol p = new Pistol(); 
		PlasmaCannon pC = new PlasmaCannon(); 
		
		SimpleTimer timer = new SimpleTimer(); 
		timer.addTimeObserver(p);
		timer.addTimeObserver(pC);
		
		//give Bob the Pistol and Sheryl the PlasmaCannon
		bob.pickUpWeapon(p); 
		sheryl.pickUpWeapon(pC); 
		
		//set distance to something in range of both weapons
		r.distance = 35; 
		
		//bob should to 5 points of damage to Sheryl
		bob.attack(sheryl, r.distance);
		assertEquals(45, sheryl.getCurrentLifePoints()); 
		
		//sheryl should to 50 points of damage to Bob
		sheryl.attack(bob, r.distance);
		assertEquals(50, bob.getCurrentLifePoints()); 
		assertEquals(pC.getCurrentAmmo(), 3); 
		
		//set distance in range of Pistol but out of range of Plasma Cannon
		r.distance = 45;  
		
		//bob should do 3 points of damage to Sheryl
		bob.attack(sheryl, r.distance);
		assertEquals(42, sheryl.getCurrentLifePoints()); 
		
		//sheryl is too far away to shoot bob, should do 0 points of damage
		//update the time, so all shots can be fired
		timer.timeChanged();
		sheryl.attack(bob, r.distance);
		assertEquals(50, bob.getCurrentLifePoints()); 
		assertEquals(pC.getCurrentAmmo(), 2); 
		
		//update the time, so all shots can be fired
		timer.timeChanged();
		//empty the Plasma Cannon clip
		sheryl.attack(bob, r.distance);
		assertEquals(pC.getCurrentAmmo(), 1); 
		//update the time, so all shots can be fired
		timer.timeChanged();
		sheryl.attack(bob, r.distance);
		assertEquals(pC.getCurrentAmmo(), 0); 
		
		//reset the distance to a ranged touch attack will work
		r.distance = 3; 
		//sheryl attacks bob hand to hand
		sheryl.attack(bob, r.distance);
		assertEquals(48, bob.getCurrentLifePoints()); 
	}
	
	/**
	 * makes sure a LifeForm can pick up a Weapon
	 * makes sure a LifeForm can only hold one Weapon at a time
	 * makes sure a Weapon can be dropped and returned to the caller
	 */
	@Test
	public void testWeaponPickUpAndDrop()
	{
		MockLifeForm bob;
		bob = new MockLifeForm("Bob", 40, 5); 
		MockWeapon gun = new MockWeapon(); 
		
		//check that bob is not holding a weapon
		assertFalse(bob.hasWeapon()); 
		
		//pick up the gun
		assertTrue(bob.pickUpWeapon(gun)); 
		
		//check that bob has a weapon
		assertTrue(bob.hasWeapon()); 
		
		//try to pick up the gun again
		assertFalse(bob.pickUpWeapon(gun)); 
		
		//drop the gun & pass it back
		MockWeapon temp = (MockWeapon)bob.dropWeapon(); 
		assertEquals(temp, gun); 
		
		//check the bob no longer has the gun
		assertFalse(bob.hasWeapon()); 
		
		//pick up the gun again
		assertTrue(bob.pickUpWeapon(gun)); 
		
		//check the bob has the gun again
		assertTrue(bob.hasWeapon()); 
		
	}

	
	/************ LAB 3 (Observer) Tests were refactored to hand ranged touch attacks *****/
	
	/**
	 * makes sure we can 
	 * * create a LifeForm with an attack strength
	 * * check the attack strength
	 * * attack another LifeForm that is within hand-to-hand attack range
	 * * see the other LifeForm take the damage
	 */
	@Test
	public void testAttack()
	{
		MockLifeForm bob;
		MockLifeForm sheryl; 
		bob = new MockLifeForm("Bob", 40, 5); 
		sheryl = new MockLifeForm("Sheryl", 50, 7); 
		
		//set the distance to an acceptable attack range
		r.distance = 4; 
		
		//check that we can get the attack strength
        assertEquals(5, bob.getAttackStrength()); 
        
        //if Bob attacks Sheryl, Sheryl should take 5 points of damage
        bob.attack(sheryl, r.distance); 
        assertEquals(45, sheryl.getCurrentLifePoints()); 
        
        //if Sheryl attacks Bob, Bob should take 7 points of damage
        sheryl.attack(bob, r.distance); 
        assertEquals(33, bob.getCurrentLifePoints()); 
	}

	
	/**
	 * makes sure that a LifeForm with zero life point does no damage
	 */
	@Test
	public void testDeadCantAttack()
	{
		MockLifeForm bob;
		MockLifeForm sheryl; 
		
		//Bob is dead
		bob = new MockLifeForm("Bob", 0, 5); 
		sheryl = new MockLifeForm("Sheryl", 50, 7); 
		
		//set the distance to an acceptable attack range
		r.distance = 4; 
	
        //if Bob attacks Sheryl, Sheryl should take 0 points of damage
        bob.attack(sheryl, r.distance); 
        assertEquals(50, sheryl.getCurrentLifePoints()); 
	}

/************ LAB 2 (Strategy) TESTS BELOW THIS POINT ******************/
	
 /** 
  * When a LifeForm is created, it should know its name and how
  * many life points it has. 
  */ 
    @Test 
    public void testInitialization() 
    { 
        MockLifeForm entity; 
        entity = new MockLifeForm("Bob", 40); 
        assertEquals("Bob", entity.getName()); 
        assertEquals(40, entity.getCurrentLifePoints()); 
    } 
    
    @Test
    public void testTakeHit()
    {
    	 MockLifeForm entity; 
         entity = new MockLifeForm("Bob", 40); 
         
         //take a 5 point hit 
         entity.takeHit(5); 
         assertEquals(35, entity.getCurrentLifePoints()); 
         
         //take a 1 point hit
         entity.takeHit(1); 
         assertEquals(34, entity.getCurrentLifePoints());
         
         //take a 34 point hit (down to 0 HP)
         entity.takeHit(34); 
         assertEquals(0, entity.getCurrentLifePoints());
         
         //take another hit (HP can't go below 0)
         entity.takeHit(5); 
         assertEquals(0, entity.getCurrentLifePoints());
    }
} 
