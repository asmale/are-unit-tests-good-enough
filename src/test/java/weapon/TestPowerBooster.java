/**
 *
 */
package weapon;

import static org.junit.Assert.*;

import java.lang.reflect.Field;

import exceptions.AttachmentException;
import exceptions.WeaponException;
import gameplay.SimpleTimer;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

/**
 * @author Dr. Alice Armstrong
 *
 */
public class TestPowerBooster
{

	/**
	 * Check the make sure that base inside of Attachment is of type Weapon
	 * 
	 * @throws AttachmentException
	 * @throws WeaponException
	 * 
	 */
	@Test
	public void testComponentType() throws AttachmentException, WeaponException
	{
		// Mock a Weapon
		Weapon w = Mockito.mock(Weapon.class);
		// Wrap it with a power booster
		PowerBooster pb = new PowerBooster(w);

		// fire to check
		pb.fire(10);

		// verify that weapon fired and is the same class as base
		Mockito.verify(w).fire(10);
		assertEquals(w.getClass(), pb.base.getClass());
	}

	/**
	 * Check if the field name is base
	 * 
	 * @throws AttachmentException
	 */
	@Test
	public void checkName() throws AttachmentException
	{
		// get all the fields
		Field[] fields = Attachment.class.getDeclaredFields();
		// check that the name is base
		assertEquals(fields[0].getName(), "base");
	}

	/**
	 * tests wrapping a PowerBooster around a ChainGun
	 *
	 * @throws WeaponException
	 * @throws AttachmentException
	 */
	@Test
	public void testPowerBoosterChainGun() throws AttachmentException, WeaponException
	{
		PowerBooster powerChain = new PowerBooster(new ChainGun());
		SimpleTimer timer = new SimpleTimer();
		timer.addTimeObserver(powerChain);
		timer.timeChanged();

		// check that the number of attachments was updated
		assertEquals(1, powerChain.getNumAttachments());

		// check toString()
		assertEquals("ChainGun +PowerBooster", powerChain.toString());

		// fire the ChainGun once at something in range (30)
		// damage should be 7 = 15*30/60
		// with the powerBooster, damage should be 7*(1+(40/40) = 14
		assertEquals(14, powerChain.fire(30));
		timer.timeChanged();

		// test at base.maxRange (60)
		// ChainGun damage is max: 15
		// with PowerBooster: 15*(1+(39/40) = 29
		assertEquals(29, powerChain.fire(60));
		timer.timeChanged();

		// test out of scope range (90)
		assertEquals(0, powerChain.fire(90));
		timer.timeChanged();

		// fire a bunch of times, then make sure that the powerboost is
		// responding
		for (int i = 0; i < 15; i++)
		{
			powerChain.fire(30);
			timer.timeChanged();
		}

		// test at base.maxRange (60)
		// ChainGun damage is max: 15
		// with PowerBooster: 15*(1+(22/40) = 23
		assertEquals(23, powerChain.fire(60));
		timer.timeChanged();

		// fire a bunch of times, then make sure that the powerboost is
		// responding
		for (int i = 0; i < 15; i++)
		{
			powerChain.fire(30);
			timer.timeChanged();
		}

		// test at base.maxRange (60)
		// ChainGun damage is max: 15
		// with PowerBooster: 15*(1+(7/40) = 17
		assertEquals(17, powerChain.fire(60));
		timer.timeChanged();
	}

	/*
	 * @Test public void testPowerBoosterScopeChainGun() {
	 * fail("test not implemented"); }
	 * 
	 * @Test public void testPowerBoosterStabilizerChainGun() {
	 * fail("test not implemented"); }
	 * 
	 * @Test public void testPOwerBoosterPowerBoosterChainGun() {
	 * fail("test not implemented"); }
	 */
}
