/**
 *
 */
package weapon;

import static org.junit.Assert.*;

import java.lang.reflect.Field;

import exceptions.AttachmentException;
import exceptions.WeaponException;
import gameplay.SimpleTimer;

import org.junit.Test;
import org.mockito.Mockito;

/**
 * @author Dr. Alice Armstrong
 *
 */
public class TestStabilizer
{

	/**
	 * Check the make sure that base inside of Attachment is of type Weapon
	 * 
	 * @throws AttachmentException
	 * @throws WeaponException
	 * 
	 */
	@Test
	public void testComponentType() throws AttachmentException, WeaponException
	{
		// Mock a Weapon
		Weapon w = Mockito.mock(Weapon.class);
		// Wrap it with a stabilizer
		Stabilizer s = new Stabilizer(w);

		// fire to check
		s.fire(10);

		// verify that weapon fired and is the same class as base
		Mockito.verify(w).fire(10);
		assertEquals(w.getClass(), s.base.getClass());
	}

	/**
	 * Check if the field name is base
	 * 
	 * @throws AttachmentException
	 */
	@Test
	public void checkName() throws AttachmentException
	{
		// get all the fields
		Field[] fields = Attachment.class.getDeclaredFields();
		// check that the name is base
		assertEquals(fields[0].getName(), "base");
	}

	/**
	 * tests wrapping a Stabilizer around a PlasmaCannon
	 * @throws WeaponException
	 */
	@Test
	public void testStabilizerPlasmaCannon() throws AttachmentException, WeaponException
	{
		Stabilizer stabilizerPlasma = new Stabilizer(new PlasmaCannon());
		SimpleTimer timer = new SimpleTimer();
		timer.addTimeObserver(stabilizerPlasma);
		timer.timeChanged();

		// check that the number of attachments was updated
		assertEquals(1, stabilizerPlasma.getNumAttachments());

		// check toString()
		assertEquals("PlasmaCannon +Stabilizer", stabilizerPlasma.toString());

		// fire the PlasmaCannon once at something in range (30)
		// power weakens with each shot
		// damage should be 50*100% = 50
		// with the stabilizer, damage should be 50*(1.25) = 62
		assertEquals(62, stabilizerPlasma.fire(30));
		timer.timeChanged();

		// test at base.maxRange (40)
		// PlasmaCannon damage is 50*75% = 37
		// with Stabilizer: 37*1.25 = 46
		assertEquals(46, stabilizerPlasma.fire(40));
		timer.timeChanged();

		// test out of base.maxRange (45)
		// PlasmaCannon damage is 0
		// with stabilizer boost = 0*1.25 = 0
		assertEquals(0, stabilizerPlasma.fire(45));
		timer.timeChanged();

		// test at base.maxRange (40)
		// PlasmaCannon damage is 50*25% = 12
		// with Stabilizer: 12*1.25 = 15
		assertEquals(15, stabilizerPlasma.fire(40));
		timer.timeChanged();

		// check that the PlasmaCannon automatically reloaded
		assertEquals(4, stabilizerPlasma.getCurrentAmmo());

		// test out of scope range (70)
		assertEquals(0, stabilizerPlasma.fire(70));
		timer.timeChanged();
	}

	/*
	 * @Test public void testStabilizerScopePlasmaGun() {
	 * fail("test not implemented"); }
	 * 
	 * @Test public void testStabilizerStabilizerPlasmaGun() {
	 * fail("test not implemented"); }
	 * 
	 * @Test public void testStabilizerPowerBoostPlasmaGun() {
	 * fail("test not implemented"); }
	 */

}
